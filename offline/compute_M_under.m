function M_under_val = compute_M_under(controller,state_set,plant,obs)
% Compute under-approximation of M that minimizes c_j on position states 
% for visulaizing an ellipsoidal over-approximation of the tube

yalmip('clear');

M_under = sdpvar(plant.n,plant.n);
c_j_2 = sdpvar(1);

ops =sdpsettings('solver','mosek');
obj_over = c_j_2 - trace(M_under)*1e-5; % logdet is nonconvex 

con_over = [];
eps = 1e-10;
N = 50;
N_cj_obs = 30;
N_test = 100;
enable_test = true;

phi_grid = linspace(-state_set.p_lim,state_set.p_lim,N);
vx_grid = linspace(-state_set.vx_lim,state_set.vx_lim,N);

% M - M_under is positive definite
for phi = phi_grid
    for vx = vx_grid
        M = inv(controller.W_fcn([0;0;phi;vx;0;0]));
        con_over = [con_over; M - M_under >= eye(6)*eps];
    end
end

%% Minimizing c_j with M_under
syms xc [6,1]
h_j_s = h_j_obs(xc,obs);
h_j_s = h_j_s(4);
%not that the function is not continously differentiable at x=obs;
%however, the considereed bounds are only required for |x-obs|>=obs(3) and
%considering subdifferentials would result in the same bound (as the norm
%does not change)
Jac_obs_fcn = matlabFunction(jacobian(h_j_s,xc),'Vars',{xc});

pos_lim = 10;
for px = linspace(-pos_lim, pos_lim,N_cj_obs)
    for pz = linspace(-pos_lim, pos_lim, N_cj_obs)
        x_pos = [px;pz;0;0;0;0];
        %compute partial h_j/partial x
        Jac_obs_ev = Jac_obs_fcn(x_pos);
        LMI = [M_under, Jac_obs_ev'; Jac_obs_ev, c_j_2];
        con_over = [con_over; LMI >= eye(7)*eps];
        % Applying Schur complement yields
        % Jac_obs_ev*M_under^{-1}*Jac_obs_ev' <= c_j
    end
end

con_over = [con_over; M_under >= 0];
optimize(con_over, obj_over);
M_under_val = value(M_under);


if enable_test
disp('checking results')

phi_grid_test = linspace(-state_set.p_lim,state_set.p_lim,N_test);
vx_grid_test = linspace(-state_set.vx_lim,state_set.vx_lim,N_test);

M_under_test = inf;

for phi = phi_grid_test
    for vx = vx_grid_test
        x_curr = [0;0;phi;vx;0;0];        
        M = inv(controller.W_fcn(x_curr));
        M_under_test = min(min(eig(M-M_under_val)), M_under_test);
    end
end

if M_under_test > 1e-10
    error('M_under is incorrect');
end
end