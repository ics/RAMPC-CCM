%  Synthesis of (robust) CCM for control of a planar quadrotor
%
%  Based on the code of Pan Zhao, UIUC, Advanced Controls Research Lab,
%  panzhao2@illinois.edu
%  https://github.com/boranzhao/robust_ccm_tube
%  for the paper:
%  P. Zhao, et al. Tube-certified trajectory tracking for nonliner systems
%  with robust control contraction metrics. Submitted to IEEE Robotics and
%  Automation Letters, 2021. 
%  Last update: Sep 9, 2021
%
%  Changes: Account for parametric uncertainty as well as disturbance
%  -deleted computation of not robust CCM (from Singh)
%  -moved constraints to load_system
%  -changed objective: instead of computing an RPI set using \alpha; set contraction rate \rho and minimize disturbance bound \overline{w}

clear all;
close all;
yalmip('clear');

% ------------------ load system parameters -------------------------------
load_system; 

%specify dynamics relevant for \dot{W}
f_phi_fcn = @(x_a) x_a(6);
f_vx_fcn = @(x_a) x_a(6)*x_a(5)-plant.g*sin(x_a(3));
Bw_phi_fcn = @(x_a) 0;
Bw_vx_fcn = @(x_a) cos(x_a(3));

% --------------------------- Parametrize W -------------------------------

W_states = x(W_states_index);
v_W = monolist(W_states,4);         % monomials of phi and vx up to degree 4
dv_W_dx = jacobian(v_W,W_states);
n_monos_W = length(v_W);
W_coef = sdpvar(n,n,n_monos_W);
W = zeros(n);
for i=1:n_monos_W
    W = W+ W_coef(:,:,i)*v_W(i);
end

% dv_W_dt could depend on d, but not on u or theta! (follows from choice of
% parametrization in W)
dv_W_dt = dv_W_dx*(f(W_states_index)+E(W_states_index)*w(1) + G(W_states_index)*w(2));  

dW_dt = zeros(n);
for i=1:n_monos_W
    dW_dt = dW_dt+ W_coef(:,:,i)*dv_W_dt(i); 
end

controller.W_lower_bound = W_lower_bound;  
state_set.W_states = W_states;
state_set.W_states_index = W_states_index;


%--------------------------- Parametrize Y ---------------------------

Y_coef = sdpvar(nu,n,n_monos_W);
Y = zeros(nu,n);
for i=1:n_monos_W
    Y = Y+ Y_coef(:,:,i)*v_W(i);
end 
vars_WY = [W_coef(:); Y_coef(:)];


% ----------------------------- Solve LMIs with SOS -----------------
tic;
[wbar_opt,mu_opt] = compute_CCM(plant,controller,W,dW_dt,Y,vars_WY,2*rho_c,state_set,10^cost_scaling); % lambda = 2*rho_c in paper from Zhao
toc;

fprintf('RCCM, rho_c = %.2f, w_bar = %.4f\n',rho_c,wbar_opt);

% ------------------------------ extract Y_fcn-----------------------------
Y_coef = value(Y_coef);  
x = x_store; % must ensure that v_W and s contain "x" instead of "x_store"

Y_fcn = zeros(nu,n);
for i=1:n_monos_W
    Y_fcn = Y_fcn+ Y_coef(:,:,i)*v_W(i);
end
Y_fcn = clean(Y_fcn,1e-10);
s = sdisplay(Y_fcn);
syms x [n 1]
syms Y_fcn [nu nu]
for i=1:nu
    for j=1:n
        Y_fcn(i,j) = eval(s{i,j});
    end
end
Y_fcn = matlabFunction(Y_fcn,'Vars',{x});

controller.Y_fcn = Y_fcn;
controller.rho_c = rho_c;
controller.wbar = wbar_opt;

% ------------------------- extract W_fcn & dW_fcn ------------------------
W_coef = value(W_coef);  
W_coef(abs(W_coef)<=1e-10) = 0;
x = x_store; % must ensure that v_W and s contain "x" instead of "x_store"
W_fcn = zeros(n);
for i=1:n_monos_W
    W_fcn = W_fcn+ W_coef(:,:,i)*v_W(i);
end
W_fcn = clean(W_fcn, 1e-10);
dv_W_dx = clean(dv_W_dx, 1e-10);
s = sdisplay(W_fcn);
s2 = sdisplay(dv_W_dx);
syms x [n 1]
syms W_fcn [n n]
for i=1:n
    for j=1:n
        W_fcn(i,j) = eval(s{i,j});
    end
end
W_fcn = matlabFunction(W_fcn,'Vars',{x});

[n1,n2]= size(dv_W_dx);
syms dv_W_dx_sym [n1 n2]

for i=1:n1
    for j=1:n2
        dv_W_dx_sym(i,j) = eval(s2{i,j});
    end
end    
dW_dphi = zeros(n);
dW_dvx = zeros(n);
for i=1:n_monos_W
    dW_dphi = dW_dphi + W_coef(:,:,i)*dv_W_dx_sym(i,1); 
    dW_dvx = dW_dvx + W_coef(:,:,i)*dv_W_dx_sym(i,2);
end
dW_dphi = matlabFunction(dW_dphi,'Vars',{x});
dW_dvx = matlabFunction(dW_dvx,'Vars',{x});
dW_dxi_fcn = @(i,x) (i==3)*dW_dphi(x)+(i==4)*dW_dvx(x);

dW_dt_fcn = @(x_a,w_a) dW_dphi(x_a)*(f_phi_fcn(x_a)+Bw_phi_fcn(x_a)*w_a) + ...
                       dW_dvx(x_a)*(f_vx_fcn(x_a)+Bw_vx_fcn(x_a)*w_a); 
%%%%%%%% for testing using (Chebyshev polynomials) approximated fcns%%%%%%%
f_vx_approx_fcn = @(x) x(6)*x(5)-plant.g*sinx(x(3));
Bw_vx_approx_fcn = @(x) cosx(x(3));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dW_dt_approx_fcn = @(x,w) dW_dphi(x)*(f_phi_fcn(x)+Bw_phi_fcn(x)*w) + ...
                   dW_dvx(x)*(f_vx_approx_fcn(x)+Bw_vx_approx_fcn(x)*w);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

controller.W_fcn = W_fcn;
controller.dW_dxi_fcn = dW_dxi_fcn;
controller.dW_dt_fcn = dW_dt_fcn;
controller.W_coeff = W_coef;
controller.dW_dt_approx_fcn = dW_dt_approx_fcn;

%% Check if our conditions for CCM hold
[rho_c_valid, w1,w2] = check_CCM(controller,plant,state_set); % should be < 0
disp('valid contraction rate: '+ string(rho_c_valid));
controller.rho_c = rho_c_valid;

%% save results
file_name = ['../data/own_rccm_' num2str(rho_c,3) '_w_' num2str(state_set.d_lim,1) '_th_' num2str(state_set.theta_prec,4) '_pd_' num2str(state_set.pd_lim,3) '_sc_' num2str(cost_scaling,3) '.mat'];
warning('off','all');
save(file_name,'plant','controller','state_set');
warning('on','all');

