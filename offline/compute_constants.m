%% offline_computation
%This file computes the constants L_D,L_{G,k};c_j based on the formulas in
%Propositions 2 and 5, involving the Jacboians
%The code requires to first compute the contraction metric M
%Note: Both the grid and the constraints are hard coded tailored to the 
%considered problem, including the parametrization of the CCM, which would 
%need to be adjusted for a different example
%%
close all;
clearvars -except file_name

% load file in which the coefficients of W,Y (contracction metric) are saved to
file_name = 'own_rccm_0.7_w_0.1_th_0.01_pd_3.14_sc_-2.5.mat';
load(file_name);

half_theta = false; % use smaller uncertainty to compare rigid tube formulation

if half_theta
    state_set.theta_lim = state_set.theta_lim/2;
end

% specify number of grid points
N = 20; 
N_cj = 100; % to compute c_j
N_cj_obs = 20; % c_j for obstacle constraints (4 layers)
N_test = 300; % for testing \overline{M} and \underline{M}
N_delta_over = 20; % for computing delta_over for rigid tube

%copy constraints for gridding from model
p_lim = state_set.p_lim;
vx_lim = state_set.vx_lim;
pd_lim = state_set.pd_lim;
vz_lim = state_set.vz_lim;
u_min = state_set.u_min;
u_max = state_set.u_max;
d_lim = state_set.d_lim;
theta_lim = state_set.theta_lim;

% obstacles: 4 obstacles with (x,y) position and radius
obs = [-1.5   -0.9    0.16;
       -1.0   -0.5    0.16;
       -0.7   -1.2    0.16;
        0.3   -1.0    0.16];
 
enable_test = false; % Enable testing \overline{M} and \underline{M} after computation with LMIs

% symbolic variables for state and input
import casadi.*
xc = SX.sym('x',plant.n);
uc = SX.sym('u',plant.nu);

Wc = controller.W_fcn(xc);
Yc = controller.Y_fcn(xc);

Mc_chol = chol(inv(Wc));

Ec = plant.E_fcn(xc); % only depends on xc
Gc = plant.G_fcn(uc); % only depends on uc

% Functions for M and M^1/2 and W^1/2
M_fcn = Function('Mc',{xc},{inv(Wc)});
M_chol_fcn = Function('M_chol',{xc},{Mc_chol});
W_chol = Function('W_chol',{xc},{chol(Wc)});

% Functions for computation of L_D and L_G
Es_fcn = Function('Es',{xc},{jacobian(Mc_chol*Ec,xc)});
Gs_fcn = Function('Gs',{xc,uc},{jacobian(Mc_chol*Gc,xc) + jacobian(Mc_chol*Gc,uc)*Yc/Wc});
% For alternative formulas with \overline{M}
dE_dx = Function('dE_dx',{xc},{jacobian(Ec,xc)});
dG_du = Function('dG_du',{xc},{jacobian(Gc,uc)});

%% Compute constant c_j
%constraint h_j(x,u)<=0
%c_j>=\|partial h_j/partial x*M^-1/2+partial h_j/partial u*Y*M^{1/2}^\top\|

disp('computing c_j for state constraints...');
% State constraints: x - state_set.Z_x <= 0 and -x -state_set.Z_x <= 0 with

% Note that the lower and upper bound have the same constant c
%We have 4 constraints -> we initialize c_j  corresponding to the four state constraints
c_x = -inf(4,1);

%grid state space (only grid phi,vx since h_j is linear and M/W is only
%parametrized by (phi,vx)
for phi = linspace(-p_lim,p_lim, N_cj)
    for vx = linspace(-vx_lim,vx_lim, N_cj)
        x = [0;0;phi;vx;0;0];
        M_chol_inv = inv(full(M_chol_fcn(x))); % Cholesky decomposition of M
        for i = 3:6
            %h_i independent of u, derivative w.r.t. x is unit vector-->
            %\|[0... 1, ... 0]*M^{1/2}|= norm of i-th row of matrix
            %M^{1/2}  
            c_x(i-2) = max(c_x(i-2),norm(M_chol_inv(i,:),2));
        end
    end
end

disp('computing c_j for input constraints...');
% Input constraints: Z_u_min <= u <= Z_u_max
Z_u_min = ones(plant.nu,1)*state_set.u_min;
Z_u_max = ones(plant.nu,1)*state_set.u_max;

c_u = -inf(plant.nu,1);
for phi = linspace(-p_lim,p_lim, N_cj)
    for vx = linspace(-vx_lim,vx_lim, N_cj)
        x = [0;0;phi;vx;0;0];
        M_chol_transp = full(M_chol_fcn(x))';
        Y = controller.Y_fcn(x);
        for i = 1:2 % for u_1 and u_2
            c_u(i) = max(c_u(i), norm(Y(i,:)*M_chol_transp));
        end
    end
end

disp('computing c_j for obstacle constraints...');
% Collision avoidance constraints: |x-x_obs|_2>=distance
nr_obs = size(obs,1);
h_j_s = -sqrt((xc(1) - obs(:,1)).^2 + (xc(2) - obs(:,2)).^2) + obs(:,3);
%not that the function is not continously differentiable at x=obs;
%however, the considereed bounds are only required for |x-obs|>=obs(3) and
%considering subdifferentials would result in the same bound (as the norm
%does not change)
Jac_obs_fcn = Function('Jac_obs',{xc},{jacobian(h_j_s,xc)});

c_obs = -inf(nr_obs,1);

pos_lim = 10;
for phi = linspace(-p_lim,p_lim, N_cj_obs)
    for vx = linspace(-vx_lim,vx_lim, N_cj_obs)
        x = [0;0;phi;vx;0;0];
        M_chol = full(M_chol_fcn(x));
        
        for px = linspace(-pos_lim, pos_lim,N_cj_obs)
            for pz = linspace(-pos_lim, pos_lim, N_cj_obs)
                x_pos = [px;pz;phi;vx;0;0];
                %compute partial h_j/partial x
                Jac_obs_ev = full(Jac_obs_fcn(x_pos));
                
                for i = 1:nr_obs
                    c_obs(i) = max(c_obs(i),norm(Jac_obs_ev(i,:)/M_chol,2));
                end
            end
        end
    end
end

%% Compute L_D
L_D = -inf;

for phi = linspace(-p_lim,p_lim,N)
    for vx = linspace(-vx_lim,vx_lim,N)
        for d = linspace(-d_lim,d_lim,2)
            x = [0;0;phi;vx;0;0];
            %in case of non-scalar disturbances, here a sum is needed
            L_D = max(L_D,norm(full(Es_fcn(x))/full(M_chol_fcn(x))*d,2));
        end
    end
end

%% Compute L_G
L_G = -inf;

phi_grid = linspace(-p_lim,p_lim,N);
vx_grid = linspace(-vx_lim,vx_lim,N);
for phi_idx = 1:length(phi_grid)
    disp(string(phi_idx) + '/' + string(length(phi_grid)));
    for vx_idx = 1:length(vx_grid)
        u_idx = 1;
        for u1 = linspace(u_min,u_max,2) % Gs linear in u
            for u2 = linspace(u_min,u_max,2)
                x = [0;0;phi_grid(phi_idx);vx_grid(vx_idx);0;0];
                u = [u1,u2];
                L_G = max(L_G,norm(full(Gs_fcn(x,u))/full(M_chol_fcn(x)),2));
            end
        end
    end
end

disp("Effective contraction rate for uncertain nonlinear system (Prop. 3): " + ...
    string(controller.rho_c-L_D-L_G*abs(theta_lim)));

%% Terminal constraints
%In the following, we check if a terminal equality constraint saitsfies the
%posed conditions with the considered uncertainty

% initial nominal parameter is 1/plant.m, initial reference is origin 
% check terminal constraint initially
z_s0 = zeros(6,1);
v_s0 = plant.m*plant.g/2;
%compute maximal \delta that satisfies constraints h_j(z_s0,v_s0) - c_j
%delta_bar <= 0
delta_bar_x = min((state_set.Z_x + z_s0(3:6))./c_x);
delta_bar_u = min((-Z_u_min + v_s0)./c_u);
delta_bar_u = min(delta_bar_u,min((Z_u_max - v_s0)./c_u));
delta_bar_0 = min(delta_bar_x,delta_bar_u);

% vertices of Theta (shifted by nominal value \bar{\theta}_0 = 1/plant.m
Theta_1 = - theta_lim;
Theta_2 = theta_lim;

%compute derivative \dot{\delta} at steady-state (for terminal condition)
w_bar_terminal = -inf;

% maximize over vertices of D and \Theta
for d = linspace(-d_lim,d_lim,2)
    for Delta_theta = linspace(Theta_1,Theta_2,2) 
        w_bar_terminal = max(w_bar_terminal, L_G*abs(Delta_theta)*delta_bar_0 + norm(full(M_chol_fcn(z_s0)) * ...
        (plant.E_fcn(z_s0)*d + plant.G_fcn(ones(plant.nu,1)*v_s0) * Delta_theta)));
    end
end

d_delta = -(controller.rho_c - L_D)*delta_bar_0 + w_bar_terminal;

if d_delta <= 0
    fprintf('Terminal invariance condition satisfied (f_delta = %5.4f <= 0)\n',d_delta);
else
    error('Terminal invariance condition not satisfied (f_delta = %5.4f > 0\n)',d_delta);
end

%% delta_over for rigid tube formulation
Theta_rigid = [1/plant.m - theta_lim, 1/plant.m + theta_lim];

delta_over = -inf;
for Theta_i = [Theta_rigid(1) Theta_rigid(2)]
    for d = [-state_set.d_lim, state_set.d_lim]
        for vx = linspace(-state_set.vx_lim, state_set.vx_lim, N_delta_over)
            for phi = linspace(-state_set.p_lim, state_set.p_lim, N_delta_over)
                x = [0;0;phi;vx;0;0]; % only phi, vx appears
                for u1 = [state_set.u_min, state_set.u_max]
                    for u2 = [state_set.u_min, state_set.u_max]
                        u = [u1, u2];
                        delta_over = max(delta_over,full(w_tilde(x,u,1/plant.m,Theta_i,d,W_chol))/ ...
                            (controller.rho_c - L_D - L_G*abs(Theta_i - 1/plant.m)));
                    end
                end
            end
        end
    end
end

%% compute M_under for tube representation
M_under = compute_M_under(controller,state_set,plant,obs);


%% save results
constants.obs = obs;
constants.L_D = L_D;
constants.L_G = L_G;
constants.c_x = c_x;
constants.c_u = c_u;
constants.c_obs = c_obs;
constants.Theta_0 = [1/plant.m - theta_lim; 1/plant.m + theta_lim];
constants.delta_bar_x = delta_bar_x;
constants.delta_bar_0 = delta_bar_0;
constants.controller_file = file_name;
constants.delta_over = delta_over;
constants.M_under = M_under;

if half_theta
    save('../data/constants_half_theta.mat','constants');
else
    save('../data/constants.mat','constants');
end