
%define model constants
plant.g = 9.81; % (m/s^2) gravity
plant.l = 0.25;  % (m) half-width of quad rotor
plant.m = 0.486;  % (kg) mass of the quad rotor. Used as the first nominal 
% parameter, not the true (unknown) value for the robust and adaptive cases
plant.J = 0.00383; % (kgm^2), moment of inertia

n= 6; nu = 2; nw = 2;
plant.n = n; plant.nu=nu; plant.nw = nw; 

%constraints
state_set.p_lim = pi/3;  % phi
state_set.pd_lim = pi;   % phi dot
state_set.vx_lim = 2;    % vx
state_set.vz_lim = 1;    % vz
state_set.d_lim = 1/10;     % d (disturbance)
state_set.theta_prec = 0.01;% percentage error parameter
state_set.theta_lim = state_set.theta_prec*1/plant.m;  % theta \in [- theta_lim, theta_lim] (already shifted by nominal theta)
state_set.Z_x = [state_set.p_lim; state_set.vx_lim; state_set.vz_lim; state_set.pd_lim];

% input constraints (u appears in Bw (Ineq (15) in Zhao))
state_set.u_min = -1;
state_set.u_max  = 3.5;

rho_c = 0.7; % initial contraction rate
cost_scaling = -2.5; % scales the coefficients of W,Y in SOS cost
W_lower_bound = 1e-2;%enforce W>=1e-2 I

%% Exact functions
% f written as a function handle: can also work when x has multiple columns
f_fcn = @(x) [x(4,:).*cos(x(3,:)) - x(5,:).*sin(x(3,:));    %px
            x(4,:).*sin(x(3,:)) + x(5,:).*cos(x(3,:));      %pz
            x(6,:);                                         %phi
            x(6,:).*x(5,:)-plant.g*sin(x(3,:));             %vx
            -x(6,:).*x(4,:)-plant.g*cos(x(3,:));            %vz
            zeros(1,size(x,2))];          
        
B = [zeros(5,2); plant.l/plant.J -plant.l/plant.J]; 

% -------------------- Define new Bw matrix (with d and theta) ------------

E_fcn = @(x) [zeros(3,1);cos(x(3));-sin(x(3));0];
G_fcn = @(u) [zeros(4,1);u(1) + u(2);0];

plant.f_fcn = f_fcn;
plant.B = B;
plant.E_fcn = E_fcn;
plant.G_fcn = G_fcn;


%% Optimization vars
%----------------- settings for searching CCM ----------------------
x = sdpvar(n,1); x_store = x;
w = sdpvar(nw,1); % disturbance and uncert param w = [d; theta]. theta is 
% shifted by the nominal parameter \bar{\theta}_0 = 1/plant.m
u = sdpvar(nu,1); % need input as variable since G(u) used for synthesis

W_states_index = [3 4]; %specify which states are used to parametrized the CCM

% ------------------ Stateconstraints for metric synthesis ----------------

%specify constraints of as ellipsoidals
state_set.box_lim = [state_set.p_lim^2-x(3)^2; state_set.vx_lim^2-x(4)^2; ...
    state_set.pd_lim^2-x(6)^2;  state_set.vz_lim^2-x(5)^2]*0.001; % scaling 0.001???
%state_set.box_lim = [state_set.box_lim; state_set.d_lim^2-w^2]; 
state_set.num_consts_4_W_states = 2;        % # constraints from box_lim that involve states on which the metric W depends
state_set.other_lim_states = [x(6);x(5)]; 
state_set.lagrange_deg_W = 4;               % for the bounds of W
state_set.lagrange_deg_ccm = 4;             % for ccm condition; had to reduce since there are 3 new variables: theta, u(1) and u(2)

u_av = mean([state_set.u_min, state_set.u_max]);
u_lim = state_set.u_max - u_av;
state_set.box_lim = [state_set.box_lim; u_lim^2 - (u(1) - u_av)^2; u_lim^2 - (u(2) - u_av)^2]; % add scaling? 

%% Approximating functions
% approximating sin_x/cos_x with Chebshev polynomials
sinx = @(x) 0.9101*(x./(pi/3)) - 0.04466*(4*(x./(pi/3)).^3 - 3*(x./(pi/3))); % 0.8799 (close to 0.9101*3/pi, -0.03915
cosx = @(x) 0.7441 -0.2499*(2*(x./(pi/3)).^2 -1);                            % 0.7652, -0.2299

% ------------------------- system dynamics -------------------------------

f_approx = @(x) [x(4)*cosx(x(3)) - x(5)*sinx(x(3));     %px
                x(4)*sinx(x(3)) + x(5)*cosx(x(3));      %pz
                x(6);                                   %phi
                x(6)*x(5)-plant.g*sinx(x(3));           %vx
                -x(6)*x(4)-plant.g*cosx(x(3));          %vz
                0];                                     %phi_dot
f = f_approx(x);

% -------------------- Define new Bw matrix (with d and theta) ------------
E_approx = @(x) [zeros(3,1);cosx(x(3));-sinx(x(3));0];
E = E_approx(x);

G = G_fcn(u);

% conditions theta in Theta and d in D
state_set.box_lim = [state_set.box_lim; state_set.d_lim^2 - w(1)^2; state_set.theta_lim^2 - w(2)^2];

df_dx = jacobian(f,x);
dE_dx = jacobian(E,x); 
dG_du = jacobian(G,u); 

B_hat = B + dG_du*1/plant.m + dG_du*w(2); % last term due to dependency of G on u
% diff dynamics: \dot{\delta_x} = A\delta_x + B_hat \delta_u + [E, G] \delta_w

A = df_dx + dE_dx*w(1); 

plant.df_dx = df_dx;
plant.A = A;
plant.B_hat = B_hat;
plant.w = w;
plant.u = u;

plant.BwXw = [E G]*w;

plant.f_approx = f_approx;
plant.E_approx = E_approx;

