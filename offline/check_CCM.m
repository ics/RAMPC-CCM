function [rho_c, W_ev_min, W_ev_max] = check_CCM(controller,plant,state_set)
%This file uses a given CCM and constraint set, and checks if the
%contraction rate rho_c is indeed satisfied
%CCM_ev_max saves the largest eigenvalue of the matrix inequality that
%should be satisfied (CCM_ev_max <= 0)
%W_ev_min, W_ev_max additionally describe the minimal and maximal 
%eigenvalue of W 

disp('Checking CCM conditions ...');
p_lim = state_set.p_lim;
pd_lim = state_set.pd_lim;
vx_lim = state_set.vx_lim;
vz_lim = state_set.vz_lim;
d_lim = state_set.d_lim;
theta_lim = state_set.theta_lim;

ctrl_N = 10; %number of grid points
p_range = linspace(-p_lim, p_lim, ctrl_N);
vx_range = linspace(-vx_lim, vx_lim, ctrl_N);
vz_range = linspace(-vz_lim, vz_lim, ctrl_N);
pd_range = linspace(-pd_lim, pd_lim, ctrl_N);

d_range = linspace(-d_lim, d_lim, 2); % Already scaled to have -1 <= d <= 1
theta_range = linspace(-theta_lim, theta_lim, 2); % d, theta enter linearly -> only check verteces

rho_c = inf;
W_ev_min = inf;
W_ev_max = -inf;

syms x_s [6,1];
syms u_s [2,1];

f = plant.f_approx(x_s); 

df_dx = matlabFunction(jacobian(f,x_s),'Vars',{x_s});

E_s = plant.E_approx(x_s);
G_s = plant.G_fcn(u_s);
dE_dx = matlabFunction(jacobian(E_s,x_s),'Vars',{x_s});
dG_du = matlabFunction(jacobian(G_s,u_s),'Vars',{u_s});

A = @(x,w) df_dx(x) + dE_dx(x)*w(1);
B_hat = @(w) plant.B + dG_du(u_s)*(1/plant.m +w(2)); %1/m=1/plan.m+\tilde{\theta}

for i = 1:length(p_range)
    disp(string(i) + '/' + string(length(p_range)));
    for j = 1:length(vx_range)
        x = [0;0;p_range(i);vx_range(j);0;0];                
        W = controller.W_fcn(x);
        Y = controller.Y_fcn(x);
        W_ev_min = min(W_ev_min,min(eig(W)));
        W_ev_max = max(W_ev_max,max(eig(W)));

        for k = 1:length(vz_range)
            for l = 1:length(pd_range)
                x = [0;0;p_range(i);vx_range(j);vz_range(k);pd_range(l)]; 
                % position states do not appear in CCM condition

                for d_idx = 1:length(d_range)
                    for theta_idx = 1:length(theta_range)
                        w = [d_range(d_idx);theta_range(theta_idx)];
                        
                        % Our condition on CCM transformed:
                        % -\dot{W} + <A_cl W> + 2 rho_c W \succeq 0
                        % = -\dot{W} + <A W + B_hat Y> + 2 rho_c W \succeq 0
                        tmp = A(x,w)*W + B_hat(w)*Y;
                        F1 = -controller.dW_dt_approx_fcn(x,w(1))+tmp+tmp';
                        %set rho_c based on generalized eigenvalue,
                        %i.e., biggest rho_c such that F_1+2*\rho_c*W<=0
                        rho_c = min(rho_c,-max(eig(F1,W))/2); % maximal ev. should be <= 0
                    end
                end
            end
        end
    end
end 


end

