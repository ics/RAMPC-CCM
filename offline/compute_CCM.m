function [wbar_opt,w_lower,max_residual] = compute_CCM(plant,controller,W,dW_dt,Y,vars_WY,lambda,state_set,scaling)
%  Compute W and Y
%
%  Based on the code of Pan Zhao, UIUC, Advanced Controls Research Lab,
%  panzhao2@illinois.edu
%  https://github.com/boranzhao/robust_ccm_tube
%  for the paper:
%  P. Zhao, et al. Tube-certified trajectory tracking for nonliner systems
%  with robust control contraction metrics. Submitted to IEEE Robotics and
%  Automation Letters, 2021. 
%  Last update: Sep 9, 2021
%
%  Compute W and Y satisfying condition (15) in paper using SOS programming  
%  with S procedure.
%  
%  Changes: Account for parametric uncertainty (theta) as well
%  - B changed to B_hat (dependent on theta)
%  - B_w depends on u
%  - Condition (15) depends on u and theta now -> modified S procedure

% x is the variable that appear in W and Y
tolerance = 1e-6;
n = plant.n;
A = plant.A;
B_hat = plant.B_hat;
BwXw = plant.BwXw;

wbar = sdpvar;

y = sdpvar(n+1,1);   % for converting a matrix-valuded SOS to a scalar-valued SOS
y1 = y(1:n);
yW = y(1:n);

tmp = A*W + B_hat*Y; % B_hat instead of B
M_pos1 = dW_dt-(tmp+tmp')-lambda*W;
yMy1 = y1'*M_pos1*y1;
%this LMI ensures contraction rate \lambda

M_pos2 = [W  , BwXw; ...
          BwXw', wbar];
yMy2 = y'*M_pos2*y;
%this LMI ensures that |BwXw|_M\leq wbar (rewritten using Schur complement)

W_lower = sdpvar; 
y_W_Wlower_y =  yW'*(W-W_lower*eye(n))*yW;
paras = [vars_WY;W_lower];

F = [];

% considering the compact set using S procedure
for i=1:length(state_set.box_lim) 
    disp(string(i) + '/' + string(length(state_set.box_lim)));
    
    % W depends on x_3, x_4 only -> 
    % corresponding entries in state_set.box_lim: 1,2
    if i==1 || i==2  
        % enforce a lower bound for W            
        [~,c_lower,v_lower] = polynomial([state_set.W_states;yW],state_set.lagrange_deg_W);

        % only take the terms quadratic in y
        index = [];

        for k=1:length(v_lower)
            if sum(degree(v_lower(k),yW)) == 2
                index = [index k];
            end
        end
        c_lower = c_lower(index); v_lower = v_lower(index);
        L_lower = c_lower'*v_lower;            
        y_W_Wlower_y =  y_W_Wlower_y-L_lower*state_set.box_lim(i);

    end
    
    % contraction condition
    % depends on x_3, x_4, x_5, x_6 and w_1, w_2 -> corresponding entries
    % in state_set.box_lim: 1,2,4,3,7,8
    if i == 1 || i ==2 || i ==3 || i == 4 || i ==7 || i == 8
        variables = [state_set.W_states;state_set.other_lim_states;plant.w;y1];

        [~,c_L1,v_L1] = polynomial(variables,state_set.lagrange_deg_ccm);

        % only take the terms quadratic in y
        index = [];
        for k=1:length(v_L1)
            if sum(degree(v_L1(k),y1)) == 2
                index = [index k];
            end
        end
        c_L1 = c_L1(index); v_L1 = v_L1(index);


        L1 = c_L1'*v_L1;

        yMy1 = yMy1 - L1*state_set.box_lim(i);        
        paras = [paras;vec(c_L1)];
        F = [F sos(L1)];
    end
    
    % Second LMI depends on x_3, x_4, u_1, u_2 and w_1, w_2 -> 
    % corresponding entries in state_set.box_lim: 1,2,5,6,7,8
    if i == 1 || i ==2 || i ==5 || i == 6 || i ==7 || i == 8
        [~,c_L2,v_L2] = polynomial([state_set.W_states;plant.w;plant.u;y],state_set.lagrange_deg_W);

        % only take the terms quadratic in y
        index = [];
        for k=1:length(v_L2)
            if sum(degree(v_L2(k),y)) == 2
                index = [index k];
            end
        end
        c_L2 = c_L2(index); v_L2 = v_L2(index);

        L2 = c_L2'*v_L2;

        yMy2 = yMy2 - L2*state_set.box_lim(i);
        paras = [paras;vec(c_lower);vec(c_L2)];
        F = [F sos(L_lower) sos(L2)];
    end    
    
    % Ineq (15) in Zhao
    % A depends on phi, vx, vz, dphi and d. B_hat depends on theta; Bw on u
    
end

% SOS constraints
F = [F  sos(yMy1) sos(yMy2) sos(y_W_Wlower_y) wbar>=tolerance W_lower >= controller.W_lower_bound];
% solve the SOS problem
ops = sdpsettings('solver','mosek','verbose',1);
disp('Problem formulation finished! Start solving...');

vars_wd = vars_WY; 
vars_wd(541:end) = vars_wd(541:end)*2; % scaling coeffs belonging to Y more
obj = norm(vars_wd,1) * scaling;
obj = obj + wbar;
%minimize maximial disturbance bound wbar + minimize the coefficients in WY
[sol,~,~,res] = solvesos(F,obj,ops,paras);
max_residual = max(res)
disp(sol.info)
if (sol.problem == 0 || sol.problem == 4) 
    %alpha_opt = value(alpha);  
    wbar_opt = value(wbar);
else
    wbar_opt = inf;
end
w_lower = value(W_lower);
end