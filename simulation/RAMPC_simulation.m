clear all
close all

import casadi.*

adaptive = true; % use adaptive method with set-membership estimation
change_traj = true; % try to change to better trajectory after 1st step, i.e., use a different initial guess to escape local minima; which is used for the adaptive MPC (the robust does not find a solution in that case)

load_MPC_config;
rng(1); 

% initial guess for position states is 
% straigth to one of the midpoints below, then straight to origin
mp1 = [-1;-1;zeros(4,1)]; % small gap
mp2 = [-0.5;-2; zeros(4,1)]; % larger gap

if half_theta
    mp2 = mp1; % go for small gap directly
end

if adaptive
    [solver,po] = setup_AMPC(constants,state_set,plant,config,geod,controller,options);
else
    [solver,po] = setup_RMPC(constants,state_set,plant,config,geod,controller,options);
end

% lower and upper bound on constraints
c_lb = [zeros(po.n_eq,1); -inf(po.n_ineq,1)];
c_ub = zeros(po.n_eq + po.n_ineq,1);

% lower and upper bound on geodesic variables
y_lb = -inf(po.n_var,1);
y_ub = inf(po.n_var,1);
y_lb(1:config.n_geod) = geod.lb;
y_ub(1:config.n_geod) = geod.ub;

x_init = config.x_init;

% parameters at time 0
if adaptive
    Theta_init = constants.Theta_0;
    par_init = [x_init;Theta_init];
else
    par_init = x_init;
end

%% initial guess for y 
theta_bar_0 = 1/plant.m; % real parameter value is config.theta_real, not 1/plant.m!
u_eq_init = plant.g/(2 * theta_bar_0);

% initial guess for position states is 
% straigth to the midpoint, then straight to origin
z_init = zeros(plant.n,config.N+1);
N_mp = round((config.N+1)/3);
z_init(:,1:N_mp) = x_init + (mp2 - x_init)*linspace(0,1,N_mp);
z_init(:,N_mp:end) = mp2 + (zeros(6,1) - mp2)*linspace(0,1,config.N+2-N_mp);

y_init = zeros(po.n_var,1);
y_init(config.n_geod+config.n_v+1:config.n_geod+config.n_v+config.n_z) = reshape(z_init,plant.n*(config.N+1),1);
y_init(config.n_geod+1:config.n_geod+config.n_v) = repmat(u_eq_init,[2*config.N,1]);

if adaptive
    y_init(config.n_geod+config.n_v+config.n_z+config.n_delta+config.n_w_bar+1) = theta_bar_0;
    y_init(config.n_geod+config.n_v+config.n_z+config.n_delta+config.n_w_bar+2)= constants.delta_bar_x;
end
    
% store results
sol_store = zeros(po.n_var,config.N_sim);
cost_store = zeros(1,config.N_sim);
par_store = zeros(length(par_init),config.N_sim);
real_traj = zeros(plant.n,config.N_sim);
real_input = zeros(plant.nu,config.N_sim);

%% solve optimization problem online
for k = 1:config.N_sim
    disp('Simulation step ' + string(k) + '/' + string(config.N_sim));
    sol = solver('x0' , y_init,...         % solution guess
                 'p', par_init,...         % parameters
                 'lbx', y_lb,...           % lower bound on x
                 'ubx', y_ub,...           % upper bound on x
                 'lbg', c_lb,...           % lower bound on g
                 'ubg', c_ub);             % upper bound on g
    
    if ~solver.stats().success
        error('Solver did not find optimal solution at step k = ' + string(k));
    end
    
    cost_store(1,k) = full(sol.f);
    y_opt = full(sol.x);
    % extract optimal state, input and tube scaling
    v_opt = reshape(y_opt(config.n_geod+1:config.n_geod+config.n_v),[plant.nu,config.N]);
    z_opt = reshape(y_opt(config.n_geod+config.n_v+1:config.n_geod+config.n_v+config.n_z),[plant.n,config.N+1]);
    delta_opt = y_opt(config.n_geod+config.n_v+config.n_z+1:config.n_geod+config.n_v+config.n_z+config.n_delta);
    
    if adaptive
        theta_bar_opt = y_opt(config.n_geod+config.n_v+config.n_z+config.n_delta+config.n_w_bar+1);
        delta_bar_opt = y_opt(config.n_geod+config.n_v+config.n_z+config.n_delta+config.n_w_bar+2);
    end

    %% simulate real dynamics using Runge-Kutta 4
    % first compute the feedback kappa based on nominal state and input
    z_rk = zeros(plant.n,3); % nominal states at ruku steps: t_k, t_k+T_s/2, t_k + T_s
    z_rk(:,1) = z_opt(:,1);
    z_rk(:,3) = z_opt(:,2);
	%also compute nominal trajectory within sampling interval, which is needed for simulating the closed-loop feedback \kappa
    if adaptive
        z_rk(:,2) = dynamics_nominal_RK(z_opt(:,1),v_opt(:,1),theta_bar_opt,plant,config.h/2);
    else
        z_rk(:,2) = dynamics_nominal_RK(z_opt(:,1),v_opt(:,1),theta_bar_0,plant,config.h/2);
    end
    
    v_rk = repmat(v_opt(:,1),[1,3]); % input is piece-wise constant
    d_rk = (rand(1,3) - 1/2)*2*state_set.d_lim; %generate random disturbances
        
    kappa_rk = @(x,z,v) kappa_full(x,z,v,geod,controller);
    [x_new, u_applied] = dynamics_real_RK(x_init,kappa_rk,z_rk,v_rk,config.theta_real,d_rk,plant,config.h);
    
    % noisy measurements for set-membership updates
    dx_noise = (rand(plant.n,1) - 1/2)*2*config.meas_noise; %generate random noise
    if adaptive 
        meas_dx = f_w(x_init,u_applied,config.theta_real,d_rk(1),plant) + dx_noise;
    end    
    
    % store important quantities
    real_traj(:,k) = x_init;
    real_input(:,k) = u_applied;
    sol_store(:,k) = y_opt;
    par_store(:,k) = par_init;
    
    if adaptive
        % set membership estimation
        non_fals = compute_non_falsified(x_init,u_applied,meas_dx,state_set.d_lim,config.meas_noise,plant);
        Theta_init = intersection(Theta_init, non_fals);
        x_init = x_new;
        par_init = [x_init;Theta_init];
    else
        x_init = x_new;
        par_init = x_init;
    end
    
    % Initialize next step
    if adaptive
        u_eq_init = plant.g/(2 * theta_bar_opt);
    else
        u_eq_init = plant.g/(2 * theta_bar_0);
    end
        
    % try to change to the shorter trajectory        
    if k == 1 && change_traj 
        z_init = zeros(plant.n,config.N+1);
        N_mp = round((config.N+1)/3);
        z_init(:,1:N_mp) = x_init + (mp1 - x_init)*linspace(0,1,N_mp);
        z_init(:,N_mp:end) = mp1 + (zeros(6,1) - mp1)*linspace(0,1,config.N+2-N_mp);
        
        y_init = zeros(po.n_var,1);
        y_init(config.n_geod+config.n_v+1:config.n_geod+config.n_v+config.n_z) = reshape(z_init,plant.n*(config.N+1),1);
        y_init(config.n_geod+1:config.n_geod+config.n_v) = repmat(u_eq_init,[2*config.N,1]);
        if adaptive
            y_init(config.n_geod+config.n_v+config.n_z+config.n_delta+config.n_w_bar+1) = theta_bar_opt;
            y_init(config.n_geod+config.n_v+config.n_z+config.n_delta+config.n_w_bar+2)=delta_bar_opt;
        end
        
    else
        % remain on current trajectory
        % warm start next step
        % theta_bar, delta_bar same as previously
        y_init = y_opt;
        % inputs, states, tube scaling and w_bar are shifted
        % inputs
        y_init(config.n_geod+1:config.n_geod+config.n_v-plant.nu) = y_opt(config.n_geod+plant.nu+1:config.n_geod+config.n_v);
        y_init(config.n_geod+config.n_v-plant.nu+1:config.n_geod+config.n_v) = [u_eq_init;u_eq_init];
        % states
        y_init(config.n_geod+config.n_v+1:config.n_geod+config.n_v+config.n_z-plant.n) = y_opt(config.n_geod+config.n_v+plant.n+1:config.n_geod+config.n_v+config.n_z);
        y_init(config.n_geod+config.n_v+config.n_z-plant.n+1:config.n_geod+config.n_v+config.n_z) = y_opt(config.n_geod+config.n_v+config.n_z-plant.n+1:config.n_geod+config.n_v+config.n_z);
        % tube scaling
        y_init(config.n_geod+config.n_v+config.n_z+1:config.n_geod+config.n_v+config.n_z+config.n_delta-1) = delta_opt(2:end);
		%append delta by value from terminal set condition
        if adaptive
            y_init(config.n_geod+config.n_v+config.n_z+config.n_delta) = delta_bar_opt; 
        else
            y_init(config.n_geod+config.n_v+config.n_z+config.n_delta) = constants.delta_bar_0;
        end
        % w_bar
        % first (N-1)*4 elements shifted
        y_init(config.n_geod+config.n_v+config.n_z+config.n_delta+1:config.n_geod+config.n_v+config.n_z+config.n_delta+config.n_w_bar-4) = ...
            y_opt(config.n_geod+config.n_v+config.n_z+config.n_delta+5:config.n_geod+config.n_v+config.n_z+config.n_delta+config.n_w_bar);
        % last 4 elements same as before
        y_init(config.n_geod+config.n_v+config.n_z+config.n_delta+config.n_w_bar-3:config.n_geod+config.n_v+config.n_z+config.n_delta+config.n_w_bar) = ...
            y_opt(config.n_geod+config.n_v+config.n_z+config.n_delta+config.n_w_bar-3:config.n_geod+config.n_v+config.n_z+config.n_delta+config.n_w_bar);
    end
end

%% post-processing
% plot closed loop trajectory and first nominal states
figure();
plot(real_traj(1,:), real_traj(2,:));
hold on;
plot(sol_store(config.n_geod+config.n_v+1,:), sol_store(config.n_geod+config.n_v+2,:),'+-');
if config.include_obstacles
    visualize_obs(constants.obs);
end

% check if real trajectory satisfies constraints
xr_min = min(real_traj,[],2)
xr_max = max(real_traj,[],2)

ur_min = min(real_input,[],2)
ur_max = max(real_input,[],2)

%% save results to file
res.y = sol_store;
res.cost = cost_store;
res.par = par_store;
res.traj = real_traj;
res.input = real_input;
res.obs = constants.obs;
res.config = config;
if adaptive
    res_case = 'res_adaptive';
elseif half_theta
    res_case = 'res_robust_half';
else
    res_case = 'res_robust';
end
    
save(['../data/sim_results/' res_case '.mat'],'res');
    