function z_new = dynamics_nominal_RK(z,v,theta_bar,params,h)
%Ruku4 discretization of dynamics f_w with nominal parameter theta_bar
k1=f_w(z,v,theta_bar,0,params);
k2=f_w(z+h/2*k1,v,theta_bar,0,params);
k3=f_w(z+h/2*k2,v,theta_bar,0,params);
k4=f_w(z+h*k3,v,theta_bar,0,params);
z_new=z+h/6*(k1+2*k2+2*k3+k4);
end
