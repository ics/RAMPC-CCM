function ret = w_tilde(z,v,theta_bar,theta_i,d_j,W_chol)
% Compute || G(u)*(theta-theta_bar) + E(x)*d ||_{M(z)}.
% v' * M(z) * v = v' * (M^1/2(z))' * M^1/2(z) * v, where M^1/2 can be any 
% matrix square root. We use M^1/2(z) = chol(W(z))' for computational 
% considerations
vec = [0;0;0;cos(z(3))*d_j;
       (v(1) + v(2))*(theta_i - theta_bar) - sin(z(3))*d_j;0];
W_chol_z = W_chol(z);
ret = norm(vec'/W_chol_z);
end