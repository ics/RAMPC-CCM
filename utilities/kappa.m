function u = kappa(v, N, gamma, gamma_s, w, controller)
% Integrate feedback control law kappa based on code from Zhao
% https://github.com/boranzhao/robust_ccm_tube
u = v;
for k=1:N+1 
    u = u + w(k)*(controller.Y_fcn(gamma(:,k))*(controller.W_fcn(gamma(:,k))\gamma_s(:,k)));
end

end