function x_dot = f_w(x,u,theta,d,plant)
% True continuous-time dynamics of system
x_dot = plant.f_fcn(x) + plant.B*u + plant.G_fcn(u)*theta + plant.E_fcn(x)*d;
end

