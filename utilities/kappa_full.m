function u = kappa_full(x,z,v,geod,controller)
% Compute geodesic and then feedback law kappa
[gamma,gamma_s] = compute_geodesic(x,z,geod);
u = kappa(v,geod.N,gamma,gamma_s,geod.w_cheby,controller);
end