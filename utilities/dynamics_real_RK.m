function [x_new, u1] = dynamics_real_RK(x,kappa,z,v,theta_real,d_real,params,h)
% Simulates the true dynamics using ruku4
% x: true state at time t_k
% kappa: function handle to feedback control law
% z: nominal states stacked [z_{t_k}, z_{t_k + h/2}, z_{t_k + h}]  
% v: nominal states stacked [v_{t_k}, v_{t_k + h/2}, v_{t_k + h}]  
% theta_real: real parameter value
% d_real: disturbance realizations stacked [d_{t_k}, d_{t_k + h/2}, d_{t_k + h}] 
% params: system parmeters
% h: ruku4 step size
% returns x_new: true state at t_{k+1}
% returns u1: applied input at time t_k

u1 = kappa(x,z(:,1),v(:,1));
k1=f_w(x,u1,theta_real,d_real(:,1),params);
u2 = kappa(x+h/2*k1,z(:,2),v(:,2));
k2=f_w(x+h/2*k1,u2,theta_real,d_real(:,2),params);
u3 = kappa(x+h/2*k2,z(:,2),v(:,2));
k3=f_w(x+h/2*k2,u3,theta_real,d_real(:,2),params);
u4 = kappa(x+h*k3,z(:,3),v(:,3));
k4=f_w(x+h*k3,u4,theta_real,d_real(:,3),params);
x_new=x+h/6*(k1+2*k2+2*k3+k4);
end