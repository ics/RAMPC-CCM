function ret = h_j_obs(x,obs)
% obstacle avoidance constraints in the form h_j_obs <= 0
ret = -sqrt((x(1) - obs(:,1)).^2 + (x(2) - obs(:,2)).^2 + eps) + obs(:,3);%eps ensures differentiability of square-root
end