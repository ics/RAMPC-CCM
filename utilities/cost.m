function ret = cost(z,v,z_ref,v_ref,Q,R)
% Quadratic cost for MPC
ret = (z-z_ref)'*Q*(z-z_ref) + (v-v_ref)'*R*(v-v_ref);
end