function [gamma, gamma_s] = compute_geodesic(x,z,initial_geodesic) %,controller)
% taken from Zhao https://github.com/boranzhao/robust_ccm_tube
n = 6;
geodesic = initial_geodesic;
N = geodesic.N; D = geodesic.D; 
persistent t_pre beq_pre copt_pre Erem_pre
if isempty(t_pre) % || (t == 0 && t_pre ~=0)
    t_pre = -3;
    beq_pre = zeros(2*n,1);
    copt_pre = zeros(n*(D+1),1);
    Erem_pre = Inf;
end

%------------ for testing -----------------
% Erem = 0;
% ue = [u;Erem];
% return;
%------------------------------------------

beq = [z;x];
% get the initial value of c corresponding to a straight line
c0 = zeros(n*(D+1),1);
%     for i =1:n    
%         c0((i-1)*(D+1)+1,1) = xStar(i);
%         c0((i-1)*(D+1)+2,1) = x(i)- xStar(i);    
%     end
% vectorized format to improve computational efficiency
i =1:n;    
c0((i-1)*(D+1)+1,1) = z;
c0((i-1)*(D+1)+2,1) = x - z;
% tic;
if norm(beq-beq_pre)<1e-8 && ~isinf(Erem_pre)
    copt = copt_pre;
    Erem = Erem_pre;
else
    % ----------------- use OPTI -----------------------------------------
    % Opt = opti('fun',geodesic.costf,'grad',geodesic.grad,'eq',geodesic.Aeq,beq,'ndec',geodesic.ndec,'x0',c0,'options',geodesic.opts_opti);
    % [copt,Erem,exitflag,info] = solve(Opt,c0);
    %     nlprob = convIpopt(Opt.prob,geodesic.opts_opti);    
    %     nlprob = convMatlab(Opt.prob,geodesic.opts_opti); 
    % --------------------------------------------------------------------

    % --------------- ipopt ----------------------------------------------
    % geodesic.nlprob.options.rl = beq;
    % geodesic.nlprob.options.ru = beq;
    % geodesic.nlprob.x0 = c0;
    % [copt,Erem,exitflag,info] = opti_ipopt(geodesic.nlprob,c0);
    % --------------------------------------------------------------
    
    % ---------------- matlab -----------------------
    geodesic.nlprob.beq = beq;
    geodesic.nlprob.x0 = c0;
    [copt,Erem,exitflag,info] = fmincon(geodesic.nlprob);
    if exitflag<0
        disp('geodesic optimization problem failed!');
    end
    %info
    % ------------------------------------------------
    beq_pre = beq;
    copt_pre = copt;
    Erem_pre = Erem;
end
% toc;
% ----------------- compute the control law -----------------------
%     tic;
%     gamma = zeros(n,N+1);
%     gamma_s = zeros(n,N+1);  
%     for i = 1:n   
%        gamma(i,:) = copt((i-1)*(D+1)+1:i*(D+1),:)'*T;       % gamma(i) is 1*(N+1); the ith elment of gamma on all the (N+1) nodes
%        gamma_s(i,:) = copt((i-1)*(D+1)+1:i*(D+1),:)'*T_dot;
%     end  
%     toc;
% vectorized format (more computationally efficient)
copt = transpose(reshape(copt,D+1,n)); % the ith row corresponds to the ith element
gamma = copt*geodesic.T;
gamma_s = copt*geodesic.Tdot;

% -------- verify whether the curve found is really a geodesic ----------
% according to equation (11) in Leung  & Manchester
%{
error = 0;
for k=1:N+1
    error = error + (gamma_s(:,k)'*(controller.W_fcn(gamma(:,k))\gamma_s(:,k))-Erem)^2*geodesic.w_cheby(k);
end
error = sqrt(error)/Erem
if error>=1e-5
     disp('The curve optimized is probably not a geodesic!');
     fprintf(1,'t= %.2e, Error = %.3e, the curve optimized is probably not a geodesic!\n',error);
     if error> 1e-2
         %pause;
     end
end
%}
% % -----------------------------------------------------------------------
end