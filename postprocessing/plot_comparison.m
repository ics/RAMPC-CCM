clear all;
close all;

load('constants_half_theta.mat');
const_half = constants;
clear constants;
load('constants.mat');
const = constants;
clear constants;
load(const.controller_file);

suffix = '.mat';
load(append('../data/sim_results/res_adaptive',suffix))
a = res;
clear simulation_results;
load(append('../data/sim_results/res_robust',suffix))
r = res;
clear simulation_results;
load(append('../data/sim_results/res_robust_half',suffix))
r_half = res;
clear simulation_results;
load(append('../data/sim_results/res_rigid_tube',suffix))
cb = res;

fontsize = 16;
linewidth = 1;
save_figs = true;
x_lim1 = [-2.4 0.8];
y_lim1 = [-2.1 0.6];


%% extracting results
a_v = reshape(a.y(a.config.n_geod+1:a.config.n_geod+a.config.n_v,:),[plant.nu,a.config.N,size(a.y,2)]);
a_z = reshape(a.y(a.config.n_geod+a.config.n_v+1:a.config.n_geod+a.config.n_v+a.config.n_z,:),[plant.n,a.config.N+1,size(a.y,2)]);
a_delta = a.y(a.config.n_geod+a.config.n_v+a.config.n_z+1:a.config.n_geod+a.config.n_v+a.config.n_z+a.config.n_delta,:);

r_v = reshape(r.y(r.config.n_geod+1:r.config.n_geod+r.config.n_v,:),[plant.nu,r.config.N,size(r.y,2)]);
r_z = reshape(r.y(r.config.n_geod+r.config.n_v+1:r.config.n_geod+r.config.n_v+r.config.n_z,:),[plant.n,r.config.N+1,size(r.y,2)]);
r_delta = r.y(r.config.n_geod+r.config.n_v+r.config.n_z+1:r.config.n_geod+r.config.n_v+r.config.n_z+r.config.n_delta,:);

r_half_v = reshape(r_half.y(r_half.config.n_geod+1:r_half.config.n_geod+r_half.config.n_v,:),[plant.nu,r_half.config.N,size(r_half.y,2)]);
r_half_z = reshape(r_half.y(r_half.config.n_geod+r_half.config.n_v+1:r_half.config.n_geod+r_half.config.n_v+r_half.config.n_z,:),[plant.n,r_half.config.N+1,size(r_half.y,2)]);
r_half_delta = r_half.y(r_half.config.n_geod+r_half.config.n_v+r_half.config.n_z+1:r_half.config.n_geod+r_half.config.n_v+r_half.config.n_z+r_half.config.n_delta,:);

cb_v = reshape(cb.y(cb.config.n_geod+1:cb.config.n_geod+cb.config.n_v,:),[plant.nu,cb.config.N,size(cb.y,2)]);
cb_z = reshape(cb.y(cb.config.n_geod+cb.config.n_v+1:cb.config.n_geod+cb.config.n_v+cb.config.n_z,:),[plant.n,cb.config.N+1,size(cb.y,2)]);
cb_delta = const_half.delta_over*ones(cb.config.n_delta,1);

% block of M_under that corresponds to position states 
M_pos = const.M_under(1:2,1:2);

%% AMPC vs. RMPC
k = 2; % plot second open-loop plan
figure(); hold on;
set(gca, 'fontname','Ariel','fontsize',fontsize);
visualize_obs(a.obs);

% propagate tube scaling delta with equality
Theta_k = a.par(plant.n+1:plant.n+2,k);
theta_bar_k = a.y(end-1,k);
d_new = delta_prop(a_z(:,1,k),a_delta(1,k),a_v(:,:,k),theta_bar_k,Theta_k,a.config.h,a.config.N,controller,state_set,plant,const);
a_delta_plot = d_new(7,:)';

d_new_r = delta_prop(r_z(:,1,k),r_delta(1,k),r_v(:,:,k),1/plant.m,const.Theta_0,r.config.h,r.config.N,controller,state_set,plant,const);
r_delta_plot = d_new_r(7,:)';

% robust
r_col = [1,0,0,1];
r_col_tr = [1,0,0,0.5];
% first plot tube
for j = 1:r.config.N
    plot_tube(r_z(:,j,k),M_pos,r_delta_plot(j),r_col_tr,linewidth);
end

plot(r_z(1,:,k), r_z(2,:,k),'-','Color',r_col,'LineWidth',linewidth,'DisplayName','Open-loop wo. adapt.');
plot(r.traj(1,:), r.traj(2,:),'--','Color',r_col,'LineWidth',linewidth,'DisplayName','Closed-loop wo. adapt.');

%adaptive
b_col = [0,0,1,1];
b_col_tr = [0,0,1,0.5];
% first plot tube
for j = 1:a.config.N
    plot_tube(a_z(:,j,k),M_pos,a_delta_plot(j),b_col_tr,linewidth);
end

plot(a_z(1,:,k), a_z(2,:,k),'-','Color',b_col,'LineWidth',linewidth,'DisplayName','Open-loop w. adapt.');
plot(a.traj(1,:), a.traj(2,:),'--','Color',b_col,'LineWidth',linewidth,'DisplayName','Closed-loop w. adapt.');

xlim(x_lim1);
ylim(y_lim1);
xlabel(' $p_1$ [m] ','Interpreter','latex','fontname', 'Arial','fontsize',fontsize);
ylabel(' $p_2$ [m] ','Interpreter','latex','fontname', 'Arial','fontsize',fontsize);  

if save_figs
    saveas(gcf,'../data/figures/AMPC-RMPC','epsc')
end

%% rigid tube vs. RMPC
k=1;
figure('DefaultAxesFontSize',fontsize); hold on;
set(gca, 'fontname','Ariel','fontsize',fontsize);
visualize_obs(a.obs);

% propagate tube scaling delta with equality
d_new_r = delta_prop(r_half_z(:,1,k),r_half_delta(1,k),r_half_v(:,:,k),1/plant.m,const_half.Theta_0,r_half.config.h,r_half.config.N,controller,state_set,plant,const_half);
r_half_delta_plot = d_new_r(7,:)';

% rigid_tube
% first plot tube
for j = 1:cb.config.N
    plot_tube(cb_z(:,j,k),M_pos,cb_delta(j,k),r_col_tr,linewidth);
end

plot(cb_z(1,:,k), cb_z(2,:,k),'-','Color',r_col,'LineWidth',linewidth,'DisplayName','Open-loop rigid tube');
plot(cb.traj(1,:), cb.traj(2,:),'--','Color',r_col,'LineWidth',linewidth,'DisplayName','Closed-loop rigid tube');

% robust
% first plot tube
for j = 1:r_half.config.N
    plot_tube(r_half_z(:,j,k),M_pos,r_half_delta_plot(j),b_col_tr,linewidth);
end
plot(r_half_z(1,:,k), r_half_z(2,:,k),'-','Color',b_col,'LineWidth',linewidth,'DisplayName','Open-loop RMPC');
plot(r_half.traj(1,:), r_half.traj(2,:),'--','Color',b_col,'LineWidth',linewidth,'DisplayName','Closed-loop RMPC');

xlabel(' $p_1$ [m] ','Interpreter','latex','fontname', 'Arial','fontsize',fontsize);
ylabel(' $p_2$ [m] ','Interpreter','latex','fontname', 'Arial','fontsize',fontsize);  

if save_figs
    saveas(gcf,'../data/figures/rigid_tube','epsc')
end
