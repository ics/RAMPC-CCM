function s = delta_prop(z0,delta0,v,theta_bar,Theta,h,N,controller,state_set,params,constants)
% Propagate tube scaling
%(tube scaling delta in MPC can also be an over-approximation, e.g., if the constraints are not active the optimal tube scaling is not unique
% --> for visualization we re-compute the smallest scaling that satisfes the conditions with equality.)

% Define joint dynamics of s = [z, delta];
f = @(s,v) f_combined(s,v,theta_bar,Theta,controller,state_set,params,constants);
s = zeros(params.n+1,N+1);
s(:,1) = [z0;delta0];
for i = 2:N+1
    s(:,i) = ruku4(f,s(:,i-1),v(:,i-1),h);
end
end


function next_state = ruku4(f,s,input,h)
%Ruku4 discretization of function f at state s with input
k1=f(s,input);
k2=f(s+h/2*k1,input);
k3=f(s+h/2*k2,input);
k4=f(s+h*k3,input);
next_state=s+h/6*(k1+2*k2+2*k3+k4);
end


function dot_combined = f_combined(s,v,theta_bar,Theta,controller,state_set,plant,constants)

z = s(1:plant.n);
delta = s(plant.n+1);

% dynamics for z
dot_combined(1:plant.n,1)= f_w(z,v,theta_bar,0,plant);

% dynamics for delta
max_mismatch = -inf;
for vert_Theta = [Theta(1), Theta(2)]
    for vert_D = [-state_set.d_lim, state_set.d_lim]
        vec = plant.G_fcn(v)*(vert_Theta - theta_bar) + plant.E_fcn(z)*vert_D;
        weighted_norm = sqrt(vec'*(controller.W_fcn(z)\vec));
        max_mismatch = max(max_mismatch, constants.L_G*abs(vert_Theta - theta_bar)*delta + weighted_norm);
    end
end

dot_combined(plant.n+1,1) = -(controller.rho_c - constants.L_D)*delta + max_mismatch;
end


