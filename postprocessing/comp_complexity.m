clearvars -except solvers po_save
close all

import casadi.*

load_MPC_config;

n_average = 100; % averaging over n_average runs of the MPC problem
options.print_time = 1; % provide timing information independently of config

% Just-in-time compilation of MPC problems
options.jit = true;
options.compiler = 'shell';
% option -o3 ignored; -o1 creates small code; -o2 creates fast code; -ot favors fast code
% see https://docs.microsoft.com/en-us/cpp/build/reference/compiler-options-listed-by-category?view=msvc-170
%options.jit_options.flags = {'-Ot'}; % does not produce faster results
options.jit_options.verbose = true;

% Save solvers and load them if available
if ~exist('solvers','var')
    disp('No saved solver found');
    tic;
    [solver_AMPC, po_AMPC] = setup_AMPC(constants,state_set,plant,config,geod,controller,options);
    toc;  
    tic;
    [solver_nominal, po_nominal] = setup_nominal_MPC(constants,state_set,plant,config,options);
    toc;
    tic;
    [solver_rigid_tube, po_rigid_tube] = setup_rigid_tube(constants,state_set,plant,config,geod,controller,options);
    toc;
    tic;
    [solver_RMPC_euler, po_RMPC_euler] = setup_RMPC_euler(constants,state_set,plant,config,geod,controller,options);
    toc;
    tic;
    [solver_RMPC, po_RMPC] = setup_RMPC(constants,state_set,plant,config,geod,controller,options);
    toc;    
    
    % save solvers
    solvers = {solver_AMPC, solver_RMPC, solver_rigid_tube, solver_RMPC_euler, solver_nominal};
    po_save = {po_AMPC, po_RMPC, po_rigid_tube, po_RMPC_euler, po_nominal};
else
    disp('Saved solver found')
    po_AMPC = po_save{1};
    po_RMPC = po_save{2};
    po_rigid_tube = po_save{3};
    po_RMPC_euler = po_save{4};
    po_nominal = po_save{5};
end

%% Initialize and solve
mp = [1;-1; zeros(4,1)]; % largest turn

% parameters at time 0
Theta_init = constants.Theta_0;
x_init = config.x_init_cc;
par_init = [x_init;Theta_init];

% initial guess
theta_bar_0 = 1/plant.m;
u_eq_init = plant.g/(2 * theta_bar_0);

% initial guess for position states is 
% straigth to the midpoint, then straight to origin
z_init = zeros(plant.n,config.N+1);
N_mp = round((config.N+1)/3);
z_init(:,1:N_mp) = x_init + (mp - x_init)*linspace(0,1,N_mp);
z_init(:,N_mp:end) = mp + (zeros(6,1) - mp)*linspace(0,1,config.N+2-N_mp);

y_init_AMPC = zeros(po_AMPC.n_var,1);
y_init_AMPC(config.n_geod+config.n_v+1:config.n_geod+config.n_v+config.n_z) = reshape(z_init,plant.n*(config.N+1),1);
y_init_AMPC(config.n_geod+1:config.n_geod+config.n_v) = repmat(u_eq_init,[2*config.N,1]);

y_init_AMPC(config.n_geod+config.n_v+config.n_z+config.n_delta+config.n_w_bar+1) = theta_bar_0;
y_init_AMPC(config.n_geod+config.n_v+config.n_z+config.n_delta+config.n_w_bar+2)= constants.delta_bar_x;

y_init_RMPC = y_init_AMPC(1:po_RMPC.n_var);
y_init_rigid_tube = y_init_AMPC(1:po_rigid_tube.n_var);
y_init_RMPC_euler = y_init_AMPC(1:po_RMPC_euler.n_var);
y_init_nominal= y_init_AMPC(config.n_geod+1:config.n_geod+config.n_v+config.n_z);

names = {'AMPC', 'RMPC', 'rigid_tube', 'RMPC_euler','nominal'};
par_inits = {par_init, x_init, x_init, x_init, x_init};

times = zeros(5,n_average);
iter = zeros(5,n_average);
for k = 1:n_average
    disp(k);
    for i = 1:5
        po = eval(append('po_',names{i}));
        y_init = eval(append('y_init_',names{i}));
        if exist('solvers','var') == 0
            solver = eval(append('solver_',names{i}));
        else
            solver = solvers{i};
        end

        % lower and upper bound on constraints and opt. variable
        c_lb = [zeros(po.n_eq,1); -inf(po.n_ineq,1)];
        c_ub = zeros(po.n_eq + po.n_ineq,1);
        y_lb = -inf(po.n_var,1);
        y_ub = inf(po.n_var,1);
        if i <= 4 % provide bounds on geodesic varaibles if not nominal MPC
            y_lb(1:config.n_geod) = geod.lb;
            y_ub(1:config.n_geod) = geod.ub;
        end

        disp(names{i});
        tic;
        res = solver('x0' , y_init,...         % solution guess
                     'p', par_inits{i},...      % parameters
                     'lbx', y_lb,...           % lower bound on x
                     'ubx', y_ub,...           % upper bound on x
                     'lbg', c_lb,...           % lower bound on g
                     'ubg', c_ub);             % upper bound on g
        toc;

        if ~solver.stats().success
            error(append('Solver did not find optimal solution for ', names{i}));
        end

        times(i,k) = solver.stats().t_proc_total;
        iter(i,k) = solver.stats().iter_count;
    end
end

means = mean(times,2);
iters = mean(iter,2);

% Save statistics in file
fileID = fopen('../data/complexity_results.txt','w');
for i = 1:5
    res = append(names{i}, ': ' ,string(means(i)), ', ', string(means(i)/means(1)*100), ...
        '%, n_var: ', string(po_save{i}.n_var), ', n_eq: ', string(po_save{i}.n_eq), ...
        ', n_ineq: ', string(po_save{i}.n_ineq),', it: ', string(iters(i)), ', t per it: ', string(means(i)./iters(i)*1000));
    disp(res);
    fprintf(fileID,'%s\n',res);
end
fclose(fileID);
