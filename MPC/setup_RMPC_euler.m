function [solver,param_out] = setup_RMPC(constants,state_set,plant,config,geod,controller,solver_options)

import casadi.*

n_v = config.n_v; n_z=config.n_z; n_delta=config.n_delta; n_w_bar = config.n_w_bar/4; n_geod=config.n_geod;
n_var = n_geod + n_v + n_z + n_delta + n_w_bar; % number of opt vars
n_par = plant.n; % number of parameters

y = MX.sym('y', n_var);
c_geod = y(1:n_geod);
v = reshape(y(n_geod+1:n_geod+n_v),[plant.nu,config.N]);
z = reshape(y(n_geod+n_v+1:n_geod+n_v+n_z),[plant.n,config.N+1]);
delta = y(n_geod+n_v+n_z+1:n_geod+n_v+n_z+n_delta);
w_bar = y(n_geod+n_v+n_z+n_delta+1:n_geod+n_v+n_z+n_delta+n_w_bar);

theta_bar = 1/plant.m;
x_t = MX.sym('par',n_par);

u_eq = plant.g/(2 * theta_bar); % input that keeps the nominal system in equilibrium

% create function for CCM
xc = SX.sym('x',plant.n);
Wc = controller.W_fcn(xc);
W_chol = Function('Mc',{xc},{chol(Wc)});

obj = MX(0);
ceq=[];
cineq=[];

%% Cost and dynamics
for i = 1:config.N
    % cost (terminal cost is zero (see report))
    obj = obj + config.h*cost(z(:,i),v(:,i),config.z_ref,[u_eq;u_eq],config.Q,config.R);
    
    % dynamics
    % Runge-Kutta 4 discretization; input is piece-wise constant
    % could treat k1,...,k4 as decision variables
    k1=f_w(z(:,i), v(:,i),theta_bar,0,plant);
    k2=f_w(z(:,i)+config.h/2*k1,v(:,i),theta_bar,0,plant);
    k3=f_w(z(:,i)+config.h/2*k2,v(:,i),theta_bar,0,plant);
    k4=f_w(z(:,i)+config.h*k3,v(:,i),theta_bar,0,plant);
    ceq = [ceq; z(:,i+1) - z(:,i) - config.h/6*(k1+2*k2+2*k3+k4)];
    
    % tube dynamics - Euler discretization
    % using w_bar as upper bound for max term
    k1_tube=f_delta_cont(delta(i),controller.rho_c,constants.L_D,w_bar(i));
    ceq = [ceq; delta(i+1) - delta(i) - config.h*k1_tube];
    
    % Enforce w_bar >= L_g*|theta^i - theta_bar|*delta + ||G*(theta^i -
    % theta_bar) + E*d^j||_M(z) (Eqn. (20))
    for k = 1:2
        for vert_d = [-state_set.d_lim, state_set.d_lim]
            % using two inequalities instead of abs 
            cineq = [cineq; -w_bar(i) + constants.L_G*(constants.Theta_0(k) - theta_bar)*delta(i) + ...
                w_tilde(z(:,i),v(:,i),theta_bar,constants.Theta_0(k),vert_d,W_chol)];
            cineq = [cineq; -w_bar(i) + constants.L_G*(-constants.Theta_0(k) + theta_bar)*delta(i) + ...
                w_tilde(z(:,i),v(:,i),theta_bar,constants.Theta_0(k),vert_d,W_chol)];
        end
    end
    
end

%% Constraints
for k = 1:config.N
    % state constraints
    cineq = [cineq; -state_set.Z_x - z(3:end,k) + constants.c_x * delta(k)];
    cineq = [cineq; -state_set.Z_x + z(3:end,k) + constants.c_x * delta(k)];
   
    % input constraints
    cineq = [cineq; state_set.u_min*ones(plant.nu,1) - v(:,k) + constants.c_u * delta(k)];
    cineq = [cineq; -state_set.u_max*ones(plant.nu,1) + v(:,k) + constants.c_u * delta(k)];
    
    if config.include_obstacles
        % obstacles 
        cineq = [cineq; h_j_obs(z(1:2,k),constants.obs) + constants.c_obs * delta(k)];
    end
end

%% Initial tube scaling constraint (V_\delta \leq \delta_0)
% geodesic equality constraints
ceq = [ceq; geod.Aeq*c_geod - [z(:,1); x_t]];

% delta_0 >= \argmax_{gamma,gamma_s} V_delta(gamma,gamma_s,x,z)
gamma = reshape(c_geod,geod.D+1,plant.n)'*geod.T;
gamma_s = reshape(c_geod,geod.D+1,plant.n)'*geod.Tdot;
cineq = [cineq; -delta(1)^2 + V_delta_squared(geod.N,gamma,gamma_s,geod.w_cheby,controller)];

%ensure that delta_0 >= 0 (delta_i >= follows for i>0)
cineq = [cineq; -delta(1)];

% ensure that gamma lies in constraint set Z_x for faster convergence
for i = 1:size(gamma,2)
    cineq = [cineq; gamma(3:end,i) - state_set.Z_x];
    cineq = [cineq; -gamma(3:end,i) - state_set.Z_x];
end

%% Terminal equality constraint
ceq = [ceq; z(:,end) - config.z_ref];
cineq = [cineq; delta(end) - constants.delta_bar_0];

%% npsol

param_out.n_eq = length(ceq);
param_out.n_ineq = length(cineq);
param_out.n_var = n_var;

con = [ceq;cineq];

nlp = struct('x', y, 'p', x_t, 'f', obj, 'g', con);
solver = nlpsol('solver', 'ipopt', nlp, solver_options);
end

function ddelta = f_delta_cont(delta,rho_c,L_D,w_bar)
ddelta = -(rho_c-L_D)*delta + w_bar;
end