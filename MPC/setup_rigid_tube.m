function [solver,param_out] = setup_rigid_tube(constants,state_set,plant,config,geod,controller,solver_options)

import casadi.*

n_v = config.n_v; n_z=config.n_z; n_geod=config.n_geod;
n_var = n_geod + n_v + n_z; % number of opt vars
n_par = plant.n; % number of parameters

y = MX.sym('y', n_var);
c_geod = y(1:n_geod);
v = reshape(y(n_geod+1:n_geod+n_v),[plant.nu,config.N]);
z = reshape(y(n_geod+n_v+1:n_geod+n_v+n_z),[plant.n,config.N+1]);

theta_bar = 1/plant.m;
x_t = MX.sym('par',n_par);

u_eq = plant.g/(2 * theta_bar); % input that keeps the nominal system in equilibrium

obj = MX(0);
ceq=[];
cineq=[];

%% Equality constraints
for i = 1:config.N
    % cost (terminal cost is zero (see report))
    obj = obj + config.h*cost(z(:,i),v(:,i),config.z_ref,[u_eq;u_eq],config.Q,config.R);
    % dynamics
    ceq = [ceq; z(:,i+1) - dynamics_nominal_RK(z(:,i),v(:,i),theta_bar,plant,config.h)];
end
% we use terminal equality constraint
ceq = [ceq; z(:,end) - config.z_ref];
% geodesic equality constraints
ceq = [ceq; geod.Aeq*c_geod - [z(:,1); x_t]];

%% Inequality constraints
% all inequalty constraints are formulated as const <= 0

% constraints
for k = 1:config.N
    % state constraints
    cineq = [cineq; -state_set.Z_x - z(3:end,k) + constants.c_x * constants.delta_over];
    cineq = [cineq; -state_set.Z_x + z(3:end,k) + constants.c_x * constants.delta_over];
   
    % input constraints
    cineq = [cineq; state_set.u_min*ones(plant.nu,1) - v(:,k) + constants.c_u * constants.delta_over];
    cineq = [cineq; -state_set.u_max*ones(plant.nu,1) + v(:,k) + constants.c_u * constants.delta_over];
    
    if config.include_obstacles
        % obstacles 
        cineq = [cineq; h_j_obs(z(1:2,k),constants.obs) + constants.c_obs * constants.delta_over];
    end
end

% delta_0 >= \argmax_{gamma,gamma_s} V_delta(gamma,gamma_s,x,z)
gamma = reshape(c_geod,geod.D+1,plant.n)'*geod.T;
gamma_s = reshape(c_geod,geod.D+1,plant.n)'*geod.Tdot;
cineq = [cineq; -constants.delta_over^2 + V_delta_squared(geod.N,gamma,gamma_s,geod.w_cheby,controller)];

% ensure that gamma lies in constraint set Z_x for faster convergence (theoretically, this is already guaranteed by Proposition 5)
for i = 1:size(gamma,2)
    cineq = [cineq; gamma(3:end,i) - state_set.Z_x];
    cineq = [cineq; -gamma(3:end,i) - state_set.Z_x];
end

%% npsol

param_out.n_eq = length(ceq);
param_out.n_ineq = length(cineq);
param_out.n_var = n_var;

con = [ceq;cineq];

nlp = struct('x', y, 'p', x_t, 'f', obj, 'g', con);
solver = nlpsol('solver', 'ipopt', nlp, solver_options);
end