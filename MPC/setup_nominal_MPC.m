function [solver,param_out] = setup_nominal_MPC(constants,state_set,plant,config,solver_options)

import casadi.*

n_v = config.N*plant.nu; n_z=(config.N+1)*plant.n;
n_var = n_v + n_z; % number of opt vars
n_par = plant.n; % number of parameters

y = MX.sym('y', n_var);
v = reshape(y(1:n_v),[plant.nu,config.N]);
z = reshape(y(n_v+1:n_v+n_z),[plant.n,config.N+1]);

x_t = MX.sym('par',n_par);

theta_bar = 1/plant.m;
u_eq = plant.g*plant.m/2; % input that keeps the nominal system in equilibrium

obj = MX(0);
ceq=[];
cineq=[];

%% Equality constraints
for i = 1:config.N
    % cost (terminal cost is zero (see report))
    obj = obj + config.h*cost(z(:,i),v(:,i),config.z_ref,[u_eq;u_eq],config.Q,config.R);
    % dynamics
    ceq = [ceq; z(:,i+1) - dynamics_nominal_RK(z(:,i),v(:,i),theta_bar,plant,config.h)];
end
% constraint on initial state
ceq = [ceq; z(:,1) - x_t];

% we use terminal equality constraint
ceq = [ceq; z(:,end) - config.z_ref];

%% Inequality constraints
% all inequalty constraints are formulated as const <= 0

% constraints
for k = 1:config.N
    % state constraints
    cineq = [cineq; -state_set.Z_x - z(3:end,k)];
    cineq = [cineq; -state_set.Z_x + z(3:end,k)];
   
    % input constraints
    cineq = [cineq; state_set.u_min*ones(plant.nu,1) - v(:,k)];
    cineq = [cineq; -state_set.u_max*ones(plant.nu,1) + v(:,k)];
    
    if config.include_obstacles
        % obstacles 
        cineq = [cineq; h_j_obs(z(1:2,k),constants.obs)];
    end
end


%% npsol

param_out.n_eq = length(ceq);
param_out.n_ineq = length(cineq);
param_out.n_var = n_var;

con = [ceq;cineq];

nlp = struct('x', y, 'p', x_t, 'f', obj, 'g', con);
solver = nlpsol('solver', 'ipopt', nlp, solver_options);
end