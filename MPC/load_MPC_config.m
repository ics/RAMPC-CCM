% load constants and configuration
half_theta = false; % use smaller uncertainty to compare rigid tube formulation

if half_theta
    load('constants_half_theta.mat');
else
    load('constants.mat');
end

% load controller
load(constants.controller_file);
if half_theta
    state_set.theta_lim = state_set.theta_lim/2;
end

geod = setup_geodesic(controller);

% planning horizon - for robust and adaptive MPC N=25, for rigid tube MPC N=35
if half_theta
    config.N = 35;
else
    config.N = 25; 
end
config.h = 0.15; % discretization step-size
config.Q = eye(6); % state cost 
config.R = 0.1*eye(2); % input cost

config.N_sim = 30; % simulation steps T_sim = N_sim * h

% Add measurement noise for velocity measurements for set membership
% estimation. f(x)_measured = f(x) + e, where e \in [-meas_noise, meas_noise]
config.meas_noise = 0.1;

config.x_init = [-2;-2;zeros(4,1)];
config.x_init_cc = [0;-1.5;zeros(4,1)]; % for comparing comp. complexity 
config.z_ref = zeros(6,1);
config.theta_real = constants.Theta_0(2); % true parameter value is on the 
% limit of the uncertain parameter set (not 1/plant.m!)

config.include_obstacles = true;

% solver options
options = struct;
options.ipopt.max_iter = 200;
options.ipopt.print_level = 0;
options.print_time = 0;

config.n_v = config.N*plant.nu;
config.n_z=(config.N+1)*plant.n;
config.n_delta=config.N+1;
config.n_w_bar = config.N*4;
config.n_geod=plant.n*(geod.D + 1);