function [solver,param_out] = setup_AMPC(constants,state_set,plant,config,geod,controller,solver_options)

import casadi.*

n_v = config.n_v; n_z=config.n_z; n_delta=config.n_delta; n_w_bar = config.n_w_bar; n_geod=config.n_geod;
n_var = n_geod + n_v + n_z + n_delta + n_w_bar + 2; % number of opt vars
n_par = plant.n + 2; % number of parameters

y = MX.sym('y', n_var);
c_geod = y(1:n_geod);
v = reshape(y(n_geod+1:n_geod+n_v),[plant.nu,config.N]);
z = reshape(y(n_geod+n_v+1:n_geod+n_v+n_z),[plant.n,config.N+1]);
delta = y(n_geod+n_v+n_z+1:n_geod+n_v+n_z+n_delta);
w_bar = reshape(y(n_geod+n_v+n_z+n_delta+1:n_geod+n_v+n_z+n_delta+n_w_bar),[4,config.N]);
theta_bar = y(n_geod+n_v+n_z+n_delta+n_w_bar+1);
delta_bar = y(n_geod+n_v+n_z+n_delta+n_w_bar+2);

% 6 + 2 parmeter for x(t), Theta_t
par = MX.sym('par',n_par);
x_t = par(1:plant.n);
Theta_t = par(end-1:end); %[min; max]

u_eq = plant.g/(2 * theta_bar); % input that keeps the nominal system in equilibrium

% create function for CCM
xc = SX.sym('x',plant.n);
Wc = controller.W_fcn(xc);
W_chol = Function('W_chol',{xc},{chol(Wc)});

obj = MX(0);
ceq=[];
cineq=[]; % all inequalty constraints are formulated as cineq <= 0

%% Cost and dynamics
for i = 1:config.N
    % cost (terminal cost is zero, simple Euler integration for stage cost)
    obj = obj + config.h*cost(z(:,i),v(:,i),config.z_ref,[u_eq;u_eq],config.Q,config.R);
    
    % dynamics
    % Runge-Kutta 4 discretization; input is piece-wise constant
    % could instead also treat k1,...,k4 as decision variables
    k1=f_w(z(:,i), v(:,i),theta_bar,0,plant);
    k2=f_w(z(:,i)+config.h/2*k1,v(:,i),theta_bar,0,plant);
    k3=f_w(z(:,i)+config.h/2*k2,v(:,i),theta_bar,0,plant);
    k4=f_w(z(:,i)+config.h*k3,v(:,i),theta_bar,0,plant);
    ceq = [ceq; z(:,i+1) - z(:,i) - config.h/6*(k1+2*k2+2*k3+k4)];
    
    % tube dynamics - Runge-Kutta 4 discretization
    % using w_bar as upper bound for max term
    k1_tube=f_delta_cont(delta(i),controller.rho_c,constants.L_D,w_bar(1,i));
    k2_tube=f_delta_cont(delta(i)+config.h/2*k1_tube,controller.rho_c,constants.L_D,w_bar(2,i));
    k3_tube=f_delta_cont(delta(i)+config.h/2*k2_tube,controller.rho_c,constants.L_D,w_bar(3,i));
    k4_tube=f_delta_cont(delta(i)+config.h*k3_tube,controller.rho_c,constants.L_D,w_bar(4,i));
    ceq = [ceq; delta(i+1) - delta(i) - config.h/6*(k1_tube+2*k2_tube+2*k3_tube+k4_tube)];
    
    % array of intermediate RK points (used for continous-time constraints on w_bar)
    z_RK = [z(:,i), z(:,i) + config.h/2*k1, z(:,i) + config.h/2*k2, z(:,i) + config.h*k3];
    delta_RK = [delta(i), delta(i) + config.h/2*k1_tube, delta(i) + config.h/2*k2_tube, delta(i) + config.h*k3_tube];
    
    % Enforce w_bar >= L_g*|theta^i - theta_bar|*delta + ||G*(theta^i -
    % theta_bar) + E*d^j||_M(z) (Eqn. (20))
    for j = 1:4
        for k = 1:2
            for vert_d = [-state_set.d_lim, state_set.d_lim]
                % using two inequalities instead of abs 
                cineq = [cineq; -w_bar(j,i) + constants.L_G*(Theta_t(k) - theta_bar)*delta_RK(j) + ...
                    w_tilde(z_RK(:,j),v(:,i),theta_bar,Theta_t(k),vert_d,W_chol)];
                cineq = [cineq; -w_bar(j,i) + constants.L_G*(-Theta_t(k) + theta_bar)*delta_RK(j) + ...
                    w_tilde(z_RK(:,j),v(:,i),theta_bar,Theta_t(k),vert_d,W_chol)];
            end
        end
    end
    
end

%% theta_bar is in Theta_0
cineq = [cineq; constants.Theta_0(1) - theta_bar];
cineq = [cineq; -constants.Theta_0(2) + theta_bar];

%% Constraints
for k = 1:config.N
    % state constraints
    cineq = [cineq; -state_set.Z_x - z(3:end,k) + constants.c_x * delta(k)];
    cineq = [cineq; -state_set.Z_x + z(3:end,k) + constants.c_x * delta(k)];
   
    % input constraints
    cineq = [cineq; state_set.u_min*ones(plant.nu,1) - v(:,k) + constants.c_u * delta(k)];
    cineq = [cineq; -state_set.u_max*ones(plant.nu,1) + v(:,k) + constants.c_u * delta(k)];
    
    if config.include_obstacles
        % obstacles 
        cineq = [cineq; h_j_obs(z(1:2,k),constants.obs) + constants.c_obs * delta(k)];
    end
end

%% Initial tube scaling constraint (V_\delta \leq \delta_0)
% geodesic equality constraints
ceq = [ceq; geod.Aeq*c_geod - [z(:,1); x_t]];

% delta_0 >= \argmax_{gamma,gamma_s} V_delta(gamma,gamma_s,x,z)
gamma = reshape(c_geod,geod.D+1,plant.n)'*geod.T;
gamma_s = reshape(c_geod,geod.D+1,plant.n)'*geod.Tdot;
cineq = [cineq; -delta(1)^2 + V_delta_squared(geod.N,gamma,gamma_s,geod.w_cheby,controller)];

%ensure that delta_0 >= 0 (delta_i >= follows for i>0)
cineq = [cineq; -delta(1)];

% ensure that gamma lies in constraint set Z_x for faster convergence
for i = 1:size(gamma,2)
    cineq = [cineq; gamma(3:end,i) - state_set.Z_x];
    cineq = [cineq; -gamma(3:end,i) - state_set.Z_x];
end

%% Terminal equality constraint (steady state does not depend on theta_bar)
ceq = [ceq; z(:,end) - config.z_ref];

% delta_bar is largest value for which the thightened constraints are 
% satisfied in the terminal set
cineq = [cineq; delta_bar - constants.delta_bar_x]; % steady-state does not depend on theta_bar

cineq = [cineq; delta_bar + (state_set.u_min - u_eq)/constants.c_u(1)];
cineq = [cineq; delta_bar + (-state_set.u_max + u_eq)/constants.c_u(2)];

% tube dynamics must be invariant for delta_bar
for i = 1:2
    for vert_d = [-state_set.d_lim, state_set.d_lim]
        cineq = [cineq; -(controller.rho_c - constants.L_D)*delta_bar + ...
            constants.L_G*(Theta_t(i) - theta_bar)*delta_bar + ...
            w_tilde(config.z_ref,[u_eq;u_eq],theta_bar,Theta_t(i),vert_d,W_chol)];
        cineq = [cineq; -(controller.rho_c - constants.L_D)*delta_bar + ...
            constants.L_G*(-Theta_t(i) + theta_bar)*delta_bar + ...
            w_tilde(config.z_ref,[u_eq;u_eq],theta_bar,Theta_t(i),vert_d,W_chol)];
    end
end

cineq = [cineq; delta(end) - delta_bar];

%% npsol
param_out.n_eq = length(ceq);
param_out.n_ineq = length(cineq);
param_out.n_var = n_var;

con = [ceq;cineq];

nlp = struct('x', y, 'p', par, 'f', obj, 'g', con);
solver = nlpsol('solver', 'ipopt', nlp, solver_options);
end

function ddelta = f_delta_cont(delta,rho_c,L_D,w_bar)
ddelta = -(rho_c-L_D)*delta + w_bar;
end
